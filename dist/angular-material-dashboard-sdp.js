/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp', [ 'ngMaterialDashboard', 'seen-sdp' ]);

'use strict';

angular.module('ngMaterialDashboardSdp')
/*
 * ماشین حالت نرم افزار
 */
.config(function ($routeProvider) {
    $routeProvider//

    // قسمت‌های مربوط به سیستم SDP و DM
    /*
     * Assets
     */
    .when('/sdp/assets', {
        controller : 'SdpAssetsCtrl',
        templateUrl : 'views/sdp-assets.html',
        name : 'Assets',
        icon : 'web_asset',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'assets',
                title : 'Assets',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'assets',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/assets/new', {
        controller : 'SdpAssetNewCtrl',
        templateUrl : 'views/sdp-asset-new.html',
        name : 'New asset',
        icon : 'add',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'assets',
                title : 'Assets',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'assets',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'new-asset',
                title : 'New asset',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'assets/new',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/asset/:assetId', {
        controller : 'SdpAssetCtrl',
        templateUrl : 'views/sdp-asset.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions, $routeParams) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'assets',
                title : 'Assets',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'assets',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'asset-' + $routeParams.assetId,
                title : 'Asset ' + $routeParams.assetId,
                type : 'link',
                priority : 10,
                visible : true,
                url : 'assets/' + $routeParams.assetId,
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    /*
     * Categories
     */
    .when('/sdp/categories', {
        controller : 'SdpCategoriesCtrl',
        templateUrl : 'views/sdp-categories.html',
        name : 'Categories',
        icon : 'folder',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'categories',
                title : 'Categories',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'categories',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/categories/new', {
        controller : 'SdpCategoryNewCtrl',
        templateUrl : 'views/sdp-category-new.html',
        name : 'New category',
        icon : 'add',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'categories',
                title : 'Categories',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'categories',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'new-category',
                title : 'New category',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'categories/new',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/category/:categoryId', {
        controller : 'SdpCategoryCtrl',
        templateUrl : 'views/sdp-category.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions, $routeParams) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'categories',
                title : 'Categories',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'categories',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'category-' + $routeParams.categoryId,
                title : 'Category ' + $routeParams.categoryId,
                type : 'link',
                priority : 10,
                visible : true,
                url : 'categories/' + $routeParams.categoryId,
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    /*
     * Tags
     */
    .when('/sdp/tags', {
        controller : 'SdpTagsCtrl',
        templateUrl : 'views/sdp-tags.html',
        name : 'Tags',
        icon : 'label',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'tags',
                title : 'Tags',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'tags',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/tags/new', {
        controller : 'SdpTagNewCtrl',
        templateUrl : 'views/sdp-tag-new.html',
        name : 'New tag',
        icon : 'add',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'tags',
                title : 'Tags',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'tags',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'new-tag',
                title : 'New tag',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'tags/new',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/tag/:tagId', {
        controller : 'SdpTagCtrl',
        templateUrl : 'views/sdp-tag.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions, $routeParams) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'tags',
                title : 'Tags',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'tags',
                groups : [ 'navigationPathMenu' ]
            });
            $actions.newAction({
                id : 'tag-' + $routeParams.tagId,
                title : 'Tag ' + $routeParams.tagId,
                type : 'link',
                priority : 10,
                visible : true,
                url : 'tags/' + $routeParams.tagId,
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    /*
     * Hide Collections tab
     */
    .when('/sdp/collections', {
        controller : 'AmdCollectionsCtrl',
        templateUrl : 'views/amd-collections.html',
        name : 'Collections',
        icon : 'storage',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'collections',
                title : 'Collections',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'collections',
                groups : [ 'navigationPathMenu' ]
            });
        }
    })//
    .when('/sdp/menu/:collectionId/item/:documentId', {
        controller : 'SdpEventMenuItemCtrl',
        templateUrl : 'views/sdp-event-menu-item.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        }
    })//
    .when('/sdp/menu/:collectionId/item/:documentId/new', {
        controller : 'SdpEventMenuItemNewCtrl',
        templateUrl : 'views/sdp-event-menu-item-new.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        }

    })//
    .when('/sdp/item/:collectionId/document/:documentId', {
        controller : 'AmdDocumentCtrl',
        templateUrl : 'views/sdp-event-document.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        }

    })//
    .when('/sdp/downloads-link', {
        controller : 'SdpDownloadedFilesLinkCtrl',
        templateUrl : 'views/sdp-downloaded-files-link.html',
        name : 'Downloaded links',
        icon : 'cloud_download',
        groups : [ 'asset-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.__account.permissions.tenant_owner;
        },
        /*
         * @ngInject
         */
        integerate : function ($actions) {
            $actions.group('navigationPathMenu').clear();
            $actions.newAction({
                id : 'amh-sdp.downloaded-files',
                title : 'downloaded files',
                type : 'link',
                priority : 10,
                visible : true,
                url : 'downloads-link',
                groups : [ 'navigationPathMenu' ]
            });
        }
    }).otherwise('/dashboard');
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * A controller to select multiple sdp-categories from a list of categories
 * @ngdoc controller
 * @name SdpCategoriesListResourceCtrl
 * @description 
 * 
 */
.controller('SdpCategoriesListResourceCtrl', function($scope, $sdp, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			loadingItems: false,
			items: [],
			selectedItems : $scope.value || []
	};

	/**
	 * Load next page
	 * 
	 * @returns promiss
	 */
	function nextPage() {
		if (ctrl.loadingItems || (requests && !requests.hasMore())) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}else{
			paginatorParameter.setPage(1);
		}
		ctrl.loadingItems = true;
		return $sdp.getCategories(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, function() {
			// handle error
		})//
		.finally(function(){
			ctrl.loadingItems = false;
		});
	}

	/**
	 * Reloads items. All states will be reset.
	 * 
	 * @returns promiss
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		return nextPage();
	}

	function toggleSelect(item){
		var index = _findIndex(item);
		if(index >= 0){
			ctrl.selectedItems.splice(index, 1);
			item.selected = false;
		}else{
			ctrl.selectedItems.push(item);
			item.selected = true;
		}
		$scope.$parent.setValue(ctrl.selectedItems);
	}
	
	function _findIndex(item){
		var elementPos = ctrl.selectedItems.map(function(x){return x.id;}).indexOf(item.id);
		return elementPos;
	}
	
	function isSelected(item){
		return ctrl.selectedItems && _findIndex(item) >= 0;
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.ctrl = ctrl;
	$scope.toggleSelect = toggleSelect;
	$scope.isSelected = isSelected;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'creation_dtime',
		'parent_id'
	];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * A controller to select one sdp-category from a list of categories
 * @ngdoc controller
 * @name SdpCategoryListResourceCtrl
 * @description 
 * 
 */
.controller('SdpCategoryListResourceCtrl', function($scope, $sdp, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			loadingItems: false,
			items: [],
			selectedItem: null
	};

	/**
	 * Load next page
	 * 
	 * @returns promiss
	 */
	function nextPage() {
		if (ctrl.loadingItems) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}else{
			paginatorParameter.setPage(1);
		}
		ctrl.loadingItems = true;
		return $sdp.getCategories(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, function() {
			// handle error
		})//
		.finally(function(){
			ctrl.loadingItems = false;
		});
	}

	/**
	 * Reloads items. All states will be reset.
	 * 
	 * @returns promiss
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		return nextPage();
	}

	function selectItem(item){
		ctrl.selectedId = item.id;
		$scope.$parent.setValue(item);
	}

	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.ctrl = ctrl;
	$scope.selectItem = selectItem;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'creation_dtime',
		'parent_id'
	];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * A controller to select one sdp-tag from a list of tags
 * @ngdoc controller
 * @name SdpTagListResourceCtrl
 * @description 
 * 
 */
.controller('SdpTagListResourceCtrl', function($scope, $sdp, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			loadingItems: false,
			items: []
	};

	/**
	 * Load next page
	 * 
	 * @returns promiss
	 */
	function nextPage() {
		if (ctrl.loadingItems) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}else{
			paginatorParameter.setPage(1);
		}
		ctrl.loadingItems = true;
		return $sdp.getTags(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, function() {
			// handle error
		})//
		.finally(function(){
			ctrl.loadingItems = false;
		});
	}

	/**
	 * Reloads items. All states will be reset.
	 * 
	 * @returns promiss
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		return nextPage();
	}

	function selectItem(item){
	    ctrl.selectedId = item.id;
		$scope.$parent.setValue(item);
	}

	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.ctrl = ctrl;
	$scope.selectItem = selectItem;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'creation_dtime'
	];
});

'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc controller
 * @name AmdContentNewCtrl
 * @description Mange content new
 */
.controller('SdpAssetNewCtrl', function ($scope, $sdp, $navigator) {

    var ctrl = {
        saving : false
    };

    function cancel() {
        $navigator.openPage('sdp/assets');
    }

    function add(conf) {
        ctrl.saving = true;
        var data = conf.model;
        $sdp.putAsset(data)//
        .then(function(asset){
            if(conf.files[0]){
                var file = conf.files[0].lfFile;
                return asset.uploadContent(file);                
            }
            return asset;
        })//
        .then(function(obj) {
            $navigator.openPage('sdp/asset/' + obj.id);
        }, function () {
            alert('Fail to create new asset.');
        })//
        .finally(function(){
            ctrl.saving = false;
        });
    }

    $scope.cancel = cancel;
    $scope.add = add;
    $scope.ctrl = ctrl;
});

'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:MainCtrl
 * @description # MainCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpAssetCtrl', function($scope, $sdp, $navigator, $routeParams, $location, $translate, $resource, QueryParameter) {

	var ctrl = {
			loadingAsset : true,
			savingAsset : false,
			loadingCategories : false,
			loadingTags : false,
			loadingRelateds : false,
			items: [],
			categories: [],
			tags: [],
			relateds: [],
			allCategories: [],
			edit: false
	};
	var asset;
	var selectedFile;


	function handlError(){
		alert($translate.instant('faile to load asset'));
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove() {
		confirm($translate.instant('The item will be deleted.'))//
		.then(function(){
			return $scope.asset.delete();//
		})//
		.then(function(){
			// TODO: maso, 1395: go to the model page
			$location.path('/assets');
		}, function(){
			alert($translate.instant('fail to delete asset'));
		});
	}

	function save(){
		ctrl.savingAsset = true;
		if(selectedFile){
			asset.updateFileOfAsset(selectedFile)//
			.then(function(){
				ctrl.edit = false;
				ctrl.savingAsset = false;
				selectedFile = false;
			}, function(){
				ctrl.edit = false;
				ctrl.savingAsset = false;		
				selectedFile = false;
				alert($translate.instant('Fail to update asset'));
			});
		}else{			
			asset.update()//
			.then(function(){
				ctrl.edit=false;
				ctrl.savingAsset = false;
			});
		}
	}
	
	function changeFile(){
		$navigator//
		.openDialog({
			templateUrl: 'views/dialogs/sdp-select-file.html',
			config: {
				files:[]
			}
		})//
		.then(function(conf){
			selectedFile = conf.files[0].lfFile;
		}, function(){
			alert($translate.instant('Fail to select file for asset.'));
		});
	}

	function loadCategories(){
		ctrl.loadingCategories = true;
		var pp = new QueryParameter();
		pp.setOrder('id', 'a');
		asset.getCategories(pp)//
		.then(function(clist){
			ctrl.categories = clist.items;
			ctrl.loadingCategories = false;
		});
	}
	
	function removeFromCategory(category){
		category.deleteAsset(asset)//
		.then(loadCategories, function(){
			alert($translate.instant('fail to load asset from category'));
		});
	}
	
	function addToCategory(){
		$resource.get('sdp-category')//
		.then(function(model){
			return model.putAsset(asset)//
			.then(function(){
				loadCategories();
			}, function(){
				alert('Fail to add new category.');
			});
		});
	}
	
	function loadTags(){
		ctrl.loadingTags = true;
		var pp = new QueryParameter();
		pp.setOrder('id', 'a');
		asset.getTags(pp)//
		.then(function(clist){
			ctrl.tags = clist.items;
			ctrl.loadingTags = false;
		});
	}
	
	function removeTag(tag){
		tag.deleteAsset(asset)//
		.then(loadTags, function(){
			// alert('fail to load categories of asset');
		});
	}
	
	function searchCategories(query){
		if(!query){
			ctrl.allCategories = [];
			return;
		}
		var pp = new QueryParameter();
		pp.setOrder('id', 'a');
		pp.setQuery(query);
		$sdp.getCategories(pp)//
		.then(function(items) {
			ctrl.allCategories = items.items;
		});
	}
	
	function addTag(){
		$resource.get('sdp-tag')//
		.then(function(model){
			return model.putAsset(asset)//
			.then(function(){
				loadTags();
			}, function(){
				alert('Fail to add new tag.');
			});
		});
	}
	
	function loadRelations(){
		ctrl.loadingRelateds = true;
		var pp = new QueryParameter();
		pp.setOrder('id', 'a');
		asset.getAssetRelations(pp)//
		.then(function(clist){
			ctrl.relateds = clist.items;
			ctrl.loadingRelateds = false;
		});
	}
	
	function removeRelation(relatedAsset){
		asset.deleteAssetRelation(relatedAsset)//
		.then(loadRelations, function(){
			alert($translate.instant('fail to delete relation of asset'));
		});
	}
	
	function addRelation(){
		$navigator.openDialog({
			templateUrl: 'views/dialogs/sdp-relation-new.html',
			config: {
				model:{}
			}
		})//
		.then(function(model){
			model.start_id = asset.id;
			return asset.putAssetRelation(model)//
			.then(function(){
				loadRelations();
			}, function(){
				alert('Fail to add new relation.');
			});
		});
	}
	
	function goToAssetPage(id){
		$location.path('/asset/'+id);
	}
	
	$scope.goToAssetPage = goToAssetPage;
	
	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.remove = remove;
	$scope.save = save;
	$scope.changeFile = changeFile;
	$scope.removeFromCategory = removeFromCategory;
	$scope.addToCategory = addToCategory;
	$scope.removeTag = removeTag;
	$scope.addTag = addTag;
	$scope.searchCategories = searchCategories;
	$scope.removeRelation = removeRelation;
	$scope.addRelation = addRelation;

	$scope.ctrl = ctrl;

	function load(){
		// Load asset
		return $sdp.getAsset($routeParams.assetId)//
		.then(function(a){
			ctrl.loadingAsset = false;
			asset = a;
			$scope.asset = a;
			return a;
		}, handlError)
		// load categories
		.then(loadCategories, function(){
			alert($translate.instant('fail to load categories'));
		})
		// load tags
		.then(loadTags, function(){
			alert($translate.instant('fail to load tags'));
		})
		// load relateds
		.then(loadRelations, function(){
			alert($translate.instant('fail to load relateds'));
		});
	}
	
	load();
	
});


'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:MainCtrl
 * @description # SdpAssetsCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpAssetsCtrl', function($scope, $sdp, $navigator, QueryParameter) {

    var paginatorParameter = new QueryParameter();
    paginatorParameter.setOrder('id', 'd');
    var requests = null;
    var ctrl = {
        loadingItems: false,
        items: []
    };

    /**
     * جستجوی درخواست‌ها
     * 
     * @param paginatorParameter
     * @returns
     */
    function find(query) {
        paginatorParameter.setQuery(query);
        paginatorParameter.setPage(1);
        reload();
    }

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.loadingItems) {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        ctrl.loadingItems = true;
        $sdp.getAssets(paginatorParameter)//
        .then(function(items) {
            requests = items;
            ctrl.items = ctrl.items.concat(requests.items);
            ctrl.loadingItems = false;
        }, function() {
            ctrl.loadingItems = false;
        });
    }

    function addAsset(){
        $navigator.openPage('/sdp/assets/new');
    }

    /**
     * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
     * 
     * @returns
     */
    function reload(){
        requests = null;
        ctrl.items = [];
        paginatorParameter.setPage(1);
        nextPage();
    }

    /**
     * درخواست مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
    function remove(object) {
        return object.delete()//
        .then(function(){
            var index = ctrl.items.indexOf(object);
            if (index > -1) {
                ctrl.items.splice(index, 1);
            }
        });
    }

    $scope.reload = reload;
    $scope.search = find;
    $scope.nextPage = nextPage;

    $scope.remove = remove;
    $scope.add = addAsset;

    $scope.ctrl = ctrl;
    $scope.paginatorParameter = paginatorParameter;
    $scope.sortKeys = [ 'id', 'name' ];
    $scope.moreActions = [ {
        title : 'New asset',
        icon : 'add',
        action : addAsset
    } ];
});

'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:SdpCategoriesCtrl
 * @description # SdpCategoriesCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpCategoriesCtrl', function($scope, $sdp, $navigator, QueryParameter) {

    var paginatorParameter = new QueryParameter();
    paginatorParameter.setOrder('id', 'd');
    paginatorParameter.setFilter('parent_id', '0');
    var requests = null;
    var ctrl = {
        loading: false,
        items: []
    };

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.loading) {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        ctrl.loading = true;
        $sdp.getCategories(paginatorParameter)//
        .then(function(items) {
            requests = items;
            ctrl.items = ctrl.items.concat(requests.items);
        }, function() {
            alert('Fail to load categories');
        })//
        .finally(function(){
            ctrl.loading = false;
        });
    }

    function addCategory(){
        $navigator.openPage('/sdp/categories/new');
    }


    /**
     * تمام حالت‌های کنترلر را دوباره مقدار دهی می‌کند.
     * 
     * @returns
     */
    function reload(){
        requests = null;
        ctrl.items = [];
        paginatorParameter.setPage(1);
        nextPage();
    }

    /**
     * دسته مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param SdpCategory
     * @returns
     */
    function remove(object) {
        return object.delete()//
        .then(function(){
            var index = ctrl.items.indexOf(object);
            if (index > -1) {
                ctrl.items.splice(index, 1);
            }
        });
    }

    /**
     * جستجوی دسته‌ها
     * 
     * @param paginatorParameter
     * @returns
     */
    function find(query) {
        paginatorParameter.setQuery(query);
        paginatorParameter.setPage(1);
        reload();
    }
    
    $scope.reload = reload;
    $scope.search = find;
    $scope.nextPage = nextPage;

    $scope.remove = remove;
    $scope.add = addCategory;

    $scope.ctrl = ctrl;
    $scope.paginatorParameter = paginatorParameter;
    $scope.sortKeys = [ 'id', 'name' ];
    $scope.moreActions = [ {
        title : 'New category',
        icon : 'add',
        action : addCategory
    } ];
    
});

'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc controller
 * @name AmdContentNewCtrl
 * @description Mange content new
 */
.controller('SdpCategoryNewCtrl', function($scope, $sdp, $navigator, $resource) {
	
	var ctrl = {
		saving : false
	};

	function cancel() {
		$navigator.openPage('sdp/categories');
	}

	function add(config) {
		ctrl.saving = true;
		var data = config.model;
		data.parent_id = ctrl.parent_id;
		$sdp.putCategory(data)//
		.then(function(obj) {
			ctrl.saving = false;
			$navigator.openPage('sdp/category/' + obj.id);
		}, function() {
			ctrl.saving = false;
			alert('Fail to create category.');
		});
	}
	
	function selectParent(){
		return $resource.get('sdp-category', {
			data: ctrl.parent
		})//
		.then(function(parent){
			ctrl.parent = parent;
			ctrl.parent_id = parent.id;
		});
	}

	$scope.cancel = cancel;
	$scope.add = add;
	$scope.selectParent = selectParent;
	$scope.ctrl = ctrl;
});

'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:SdpCategoryCtrl
 * @description # SdpCategoryCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpCategoryCtrl', function($scope, $sdp, $resource, 
		$routeParams, $location, QueryParameter) {

	var ctrl = {
			loadingCategory : true,
			loadingChilds : false,
			savingCategory : false,
			items: [],
			childs: [],
			edit: false
	};
	var category;
	var childsPP = new QueryParameter();
	childsPP.setFilter('parent_id', $routeParams.categoryId);
    var childsPaginator = null;


	function handlError(){
		alert('fail to load category');
	}

	/**
     * دسته مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
	function remove() {
		confirm('delete category ' + $scope.category.id + '?')//
		.then(function(){
		    return $scope.category.delete();//
		})//
		.then(function(){
			// TODO: maso, 1395: go to the model page
			$location.path('/categories');
		}, function(error){
		    alert('fail to delete category:' + error);
		});
	}

	function save(){
		ctrl.savingCategory = true;
		category.update()//
		.then(function(){
			ctrl.edit=false;
			ctrl.savingCategory = false;
		});
	}

	function nextChilds(){
	    if (ctrl.loadingChilds || ctrl.loadingCategory) {
            return false;
        }
        if (childsPaginator && !childsPaginator.hasMore()) {
            return false;
        }
        if (childsPaginator) {
            childsPP.setPage(childsPaginator.next());
        }
        ctrl.loadingChilds = true;
        $sdp.getCategories(childsPP)//
        .then(function(clist){
            childsPaginator = clist;
            ctrl.childs = ctrl.childs.concat(clist.items);
            ctrl.loadingChilds = false;
        });
    }
	
	function loadCategory(){
    	// Load category
        $sdp.getCategory($routeParams.categoryId)//
        .then(function(a){
            category = a;
            $scope.category = a;
            ctrl.loadingCategory = false;
            return a;
        }, handlError)//
        .then(nextChilds);
	}
	
	function selectParent(){
        return $resource.get('sdp-category', {
            data: ctrl.parent
        })//
        .then(function(parent){
            $scope.category.parent_id = parent.id;
        });
    }
	
	/*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
	$scope.remove = remove;
	$scope.save = save;
	$scope.nextChilds = nextChilds;
	$scope.selectParent = selectParent;

	$scope.ctrl = ctrl;

	loadCategory();
});


/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:LinkPaymentCtrl
 * @description # LinkPaymentCtrl Controller of the amhSdp
 */
.controller('SdpDownloadedFilesLinkCtrl', function($scope, $sdp, QueryParameter) {
	$scope.mainWaiting = true;
    $scope.flag = false;
    var paginatorParameter = new QueryParameter();
	var requests = null;
	
       
	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if ($scope.loadingLinks) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		$scope.loadingLinks = true;
		$sdp.getLinks(paginatorParameter)//
		.then(function(items) {
            $scope.mainWaiting = false;
            $scope.error = null;
            requests = items;
            $scope.items = $scope.items.concat(requests.items);
            if(items.length===0){
                $scope.flag = true;
            }
		}, function(error) {
            $scope.error = error;
		})
        .finally(function(){
            $scope.loadingLinks = false;
        });
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload() {
		requests = null;
		$scope.items = [];
		nextPage();
	}

//	function add() {};
//	function remove() {};
//	function open() {}
	
	
	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys = [ 'id', 'creation_dtime' ];

});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc controller
 * @name AmdContentNewCtrl
 * @description Mange content new
 */
.controller('SdpTagNewCtrl', function($scope, $sdp, $navigator) {

    var ctrl = {
        saving : false
    };

    function cancel() {
        $navigator.openPage('/sdp/tags');
    }

    function add(conf) {
        ctrl.saving = true;
        var data = conf.model;
        $sdp.putTag(data)//
        .then(function(obj) {
            ctrl.saving = false;
            $navigator.openPage('/sdp/tag/' + obj.id);
        }, function() {
            ctrl.saving = false;
            alert('Fail to create new tag.');
        });
    }

    $scope.cancel = cancel;
    $scope.add = add;
    $scope.ctrl = ctrl;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:SdpTagCtrl
 * @description # SdpTagCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpTagCtrl', function($scope, $sdp, $navigator, $routeParams, $location, $translate) {

    var ctrl = {
        loadingTag : true,
        savingTag : false,
        items: [],
        childs: [],
        edit: false
    };
    var tag;

    function handlError(){
        alert($translate.instant('faile to load asset'));
    }
    
    /**
     * دسته مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
    function remove() {
        confirm('delete tag ' + $scope.tag.id +'?')//
        .then(function(){
            return $scope.tag.delete();//
        })//
        .then(function(){
            // TODO: maso, 1395: go to the model page
            $location.path('/tags');
        }, function(error){
            alert('fail to delete tag:' + error.message);
        });
    }

    function save(){
        ctrl.savingTag = true;
        tag.update()//
        .then(function(){
            ctrl.edit=false;
            ctrl.savingTag = false;
        });
    }

    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
    $scope.remove = remove;
    $scope.save = save;

    $scope.ctrl = ctrl;

    function load(){        
        // Load tag
        return $sdp.getTag($routeParams.tagId)//
        .then(function(a){
            tag = a;
            $scope.tag = a;
            ctrl.loadingTag = false;
            return a;
        }, handlError);
    }

    load();
    
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')

/**
 * @ngdoc function
 * @name saasdmCpanelApp.controller:SdpTagsCtrl
 * @description # SdpTagsCtrl Controller of the saasdmCpanelApp
 */
.controller('SdpTagsCtrl', function($scope, $sdp, $navigator, QueryParameter) {

    var paginatorParameter = new QueryParameter();
    paginatorParameter.setOrder('id', 'd');
    var requests = null;
    var ctrl = {
        loadingItems: false,
        items: []
    };

    /**
     * جستجوی دسته‌ها
     * 
     * @param paginatorParameter
     * @returns
     */
    function find(query) {
        paginatorParameter.setQuery(query);
        paginatorParameter.setPage(1);
        reload();
    }

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.loadingItems) {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        ctrl.loadingItems = true;
        $sdp.getTags(paginatorParameter)//
        .then(function(items) {
            requests = items;
            ctrl.items = ctrl.items.concat(requests.items);
            ctrl.loadingItems = false;
        }, function() {
            ctrl.loadingItems = false;
        });
    }

    function addTag(){
        $navigator.openPage('/sdp/tags/new');
    }


    /**
     * تمام حالت‌های کنترلر را دوباره مقدار دهی می‌کند.
     * 
     * @returns
     */
    function reload(){
        requests = null;
        ctrl.items = [];
        paginatorParameter.setPage(1);
        nextPage();
    }

    /**
     * دسته مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param SdpTag
     * @returns
     */
    function remove(object) {
        return object.delete()//
        .then(function(){
            var index = ctrl.items.indexOf(object);
            if (index > -1) {
                ctrl.items.splice(index, 1);
            }
        });
    }

    $scope.items = [];
    $scope.reload = reload;
    $scope.search = find;
    $scope.nextPage = nextPage;

    $scope.remove = remove;
    $scope.add = addTag;

    $scope.ctrl = ctrl;
    $scope.paginatorParameter = paginatorParameter;
    $scope.sortKeys = [ 'id', 'name' ];
    $scope.moreActions = [ {
        title : 'New tag',
        icon : 'add',
        action : addTag
    } ];

});

  /* jslint todo: true */
  /* jslint xxx: true */
  /* jshint -W100 */
  'use strict';

  angular.module('ngMaterialDashboardSdp')

          /**
           * @ngdoc directive
           * @name amhSdp.directive:sdpAssetFilter
           * @description # sdpAssetFilter
           */
          .directive('sdpDownloadedLink', function () {
              return {
                  restrict: 'E',
                  templateUrl: 'views/directives/sdp-downloaded-link.html',
                  scope: {
                      item: '='
                  },
                  controller: function ($scope, /*$sdp, */$usr) {
//                      $sdp.asset($scope.item.asset)//
//                              .then(function (asset) {
//                                  $scope.fileName = asset.name;
//                              });//
                      $usr.getAccount($scope.item.user)//
                              .then(function (user) {
                                  $scope.user = user;
                              });//
                  }
              };
          });


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($navigator) {
	$navigator
	.newGroup({
		id: 'asset-management',
		title: 'Asset management',
		description: 'Manage all assets, categories and tags in the system.',
		icon: 'web_asset',
		hidden: '!app.user.tenant_owner',
		priority: 5
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSdp')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($resource) {
	$resource.newPage({
		label: 'Category list',
		type:'sdp-category-list',
		templateUrl: 'views/resources/sdp-category-list.html',
		controller: 'SdpCategoryListResourceCtrl',
		tags: ['sdp-category']
	});
	$resource.newPage({
		label: 'Categories list',
		type:'sdp-categories-list',
		templateUrl: 'views/resources/sdp-categories-list.html',
		controller: 'SdpCategoriesListResourceCtrl',
		tags: ['sdp-category-list']
	});
	
	$resource.newPage({
        label: 'Tag list',
        type:'sdp-tag-list',
        templateUrl: 'views/resources/sdp-tag-list.html',
        controller: 'SdpTagListResourceCtrl',
        tags: ['sdp-tag']
    });
	
});


angular.module('ngMaterialDashboardSdp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/dialogs/sdp-category-select.html',
    "<md-dialog ng-controller=SdpCategoriesCtrl aria-label=Category> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Select category</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.selected)> <wb-icon aria-label=select>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content ng-init=nextPage() amd-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list flex> <md-list-item class=md-3-line ng-click=\"ctrl.selected = category\" ng-class=\"{true:'md-raised md-primary', false:''}[ctrl.selected.id === category.id]\" ng-style=\"{'background-color': ctrl.selected.id === category.id ? '#cce2ff' : ''}\" ng-repeat=\"category in ctrl.items\"> <div class=md-list-item-text layout=column> <h4>ID: {{category.id}}</h4> <h3>Name: {{category.name}}</h3> <h4>Parent: {{category.parent}}</h4> <p>Description: {{category.description}}</p> </div> <md-divider></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-dialog-content> <md-dialog-actions layout=row> <span flex></span> <form ng-submit=search(temp.query)> <md-input-container class=\"md-icon-float md-icon-right md-block\"> <label translate>Search</label> <wb-icon>search</wb-icon> <input ng-model=temp.query> </md-input-container> <input type=submit hide> </form> </md-dialog-actions> </md-dialog>"
  );


  $templateCache.put('views/dialogs/sdp-event-document-new.html',
    "<md-dialog aria-label=\"Menu Item\" ng-cloak> <form name=myForm flex layout=column ng-action=answer(config.model)> <md-toolbar> <div class=md-toolbar-tools> <h2>New Event Document</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model)> <wb-icon aria-label=close>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding> <md-input-container ng-repeat=\"key in ['title', 'description', 'thumbnail', 'content']\" flex> <label translate>{{key}}</label> <input ng-model=config.model[key]> </md-input-container>                   </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/sdp-relation-new.html',
    "<md-dialog ng-controller=SdpAssetsCtrl aria-label=AssetRelation> <md-toolbar> <div class=md-toolbar-tools> <h2>New Relation</h2> <span flex></span> <form ng-submit=search(temp.query)> <md-input-container class=\"md-icon-float md-icon-right md-block\"> <label translate>Search</label> <input ng-model=temp.query> </md-input-container> <input type=submit hide> </form> <md-button class=md-icon-button ng-disabled=\"!config.model.type || !config.model.end_id\" ng-click=answer(config.model)> <wb-icon aria-label=close>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content ng-init=nextPage() amd-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list flex> <md-radio-group ng-model=config.model.end_id> <md-list-item class=md-3-line ng-click=\"config.model.end_id = asset.id\" ng-class=\"{true:'md-raised md-primary', false:''}[config.model.end_id === asset.id]\" ng-style=\"{'background-color': config.model.end_id === asset.id ? '#cce2ff' : ''}\" ng-repeat=\"asset in ctrl.items\"> <div class=md-list-item-text layout=column> <h4>ID: {{asset.id}}</h4> <h3>Name: {{asset.name}}</h3> <p>Description: {{asset.description}}</p> </div> <md-radio-button ng-value=asset.id class=\"md-secondary md-primary\"> </md-radio-button> <md-divider></md-divider> </md-list-item> </md-radio-group>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-dialog-content> <md-dialog-actions> <form layout=row> <md-input-container> <label>Type</label> <input ng-model=config.model.type required> </md-input-container> <md-input-container> <label>Asset Id</label> <input ng-model=config.model.end_id ng-disabled required> </md-input-container> <md-input-container> <label>Description</label> <input ng-model=config.model.description> </md-input-container> <md-checkbox ng-model=config.model.bidirectional aria-label=bidirectional> {{'bidirectional' | translate}} </md-checkbox> </form> </md-dialog-actions> </md-dialog>"
  );


  $templateCache.put('views/dialogs/sdp-select-file.html',
    "<md-dialog aria-label=\"Select File\" ng-cloak> <form flex layout=column ng-action=answer(config)> <md-toolbar> <div class=md-toolbar-tools> <h2>Select File</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config)> <wb-icon aria-label=close>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding> <md-input-container> <lf-ng-md-file-input lf-files=config.files progress preview drag></lf-ng-md-file-input> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/sdp-tag-select.html',
    "<md-dialog ng-controller=SdpTagsCtrl aria-label=Tag> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Select tag</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.selected)> <wb-icon aria-label=select>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content ng-init=nextPage() amd-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list flex> <md-list-item class=md-3-line ng-click=\"ctrl.selected = tag\" ng-class=\"{true:'md-raised md-primary', false:''}[ctrl.selected.id === tag.id]\" ng-style=\"{'background-color': ctrl.selected.id === tag.id ? '#cce2ff' : ''}\" ng-repeat=\"tag in ctrl.items\"> <div class=md-list-item-text layout=column> <h4>ID: {{tag.id}}</h4> <h3>Name: {{tag.name}}</h3> <p>Description: {{tag.description}}</p> </div> <md-divider></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-dialog-content> <md-dialog-actions layout=row> <span flex></span> <form ng-submit=search(temp.query)> <md-input-container class=\"md-icon-float md-icon-right md-block\"> <label translate>Search</label> <wb-icon>search</wb-icon> <input ng-model=temp.query> </md-input-container> <input type=submit hide> </form> </md-dialog-actions> </md-dialog>"
  );


  $templateCache.put('views/directives/sdp-downloaded-link.html',
    "<md-list-item class=md-3-line ng-href={{}}> <wb-icon ng-if=item.active>done</wb-icon> <wb-icon ng-if=!item.active>cancel</wb-icon> <div class=md-list-item-text layout=column> <h4>{{fileName}}</h4> <h4 ng-if=\"!(user.first_name && user.last_name)\" translate>Provider: unknown </h4> <h4 ng-if=\"user.first_name && user.last_name\" translate>Provider: {{user.first_name + ' ' + user.last_name}} </h4> <p><span translate>Creation date</span>: {{item.creation_dtime| mbDate}} - <span translate>Expiry date</span>: {{item.expiry| mbDate}}</p> </div> <md-button class=md-icon-button> <wb-icon ng-if=item.active>delete</wb-icon> </md-button> </md-list-item>"
  );


  $templateCache.put('views/resources/sdp-categories-list.html',
    "<div flex layout=column>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-more-actions=moreActions> </mb-pagination-bar> <md-content mb-preloading=ctrl.loadingItems mb-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-click=toggleSelect(object) class=md-3-line> <wb-icon>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.id}}</h3> <h4>{{object.name}}</h4> <p>{{object.description}}</p> </div> <md-checkbox class=md-secondary ng-init=\"object.selected = isSelected(object)\" ng-model=object.selected ng-click=toggleSelect(object)> </md-checkbox> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.loadingItems && !(ctrl.items && ctrl.items.length)\"> <h2 translate>Empty list</h2> </div> </md-content> </div>"
  );


  $templateCache.put('views/resources/sdp-category-list.html',
    "<md-content mb-preloading=ctrl.loadingItems mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column ng-if=\"ctrl.items.length == 0 && !ctrl.loadingItems\"> <p style=\"text-align: center\" translate>List is empty.</p> </div> <md-list flex> <md-radio-group ng-model=ctrl.selectedId> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-click=selectItem(pobject)> <wb-icon>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-radio-button ng-value=pobject.id class=\"md-secondary md-primary\"> </md-radio-button> <md-divider md-inset></md-divider> </md-list-item> </md-radio-group> </md-list> </md-content>"
  );


  $templateCache.put('views/resources/sdp-tag-list.html',
    "<md-content mb-preloading=ctrl.loadingItems mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column ng-if=\"ctrl.items.length == 0 && !ctrl.loadingItems\"> <p style=\"text-align: center\" translate>List is empty.</p> </div> <md-list flex> <md-radio-group ng-model=ctrl.selectedId> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-click=selectItem(pobject)> <wb-icon>label</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-radio-button ng-value=pobject.id class=\"md-secondary md-primary\"> </md-radio-button> <md-divider md-inset></md-divider> </md-list-item> </md-radio-group> </md-list> </md-content>"
  );


  $templateCache.put('views/sdp-asset-new.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=ctrl.saving mb-title=\"{{'New asset'| translate}}\" layout=column> <form name=assetForm ng-action=add(config) layout=column flex> <md-input-container> <label>Name</label> <input ng-model=config.model.name> </md-input-container> <md-input-container> <label>Description</label> <input ng-model=config.model.description> </md-input-container> <md-input-container> <label>Price</label> <input ng-model=config.model.price> </md-input-container> <md-input-container> <lf-ng-md-file-input lf-files=config.files progress preview drag></lf-ng-md-file-input> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel()>  <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=add(config)>  <span translate=\"\">Add</span> </md-button> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-asset.html',
    "<md-content layout-margin flex> <mb-titled-block mb-progress=\"ctrl.loadingAsset || ctrl.savingAsset\" mb-title=\"{{'Asset'| translate}}\" layout=column> <section ng-show=!ctrl.edit layout=column> <table> <tr> <td>ID </td> <td>: {{asset.id}}</td> </tr> <tr> <td>Name </td> <td>: {{asset.name}}</td> </tr> <tr> <td>Type </td> <td>: {{asset.type}}</td> </tr> <tr ng-if=\"asset.type === 'file'\"> <td>Price </td> <td>: {{asset.price}}</td> </tr> <tr> <td>Description </td> <td>: {{asset.description}}</td> </tr> <tr> <td>Creation Date </td> <td>: {{asset.creation_dtime}}</td> </tr> <tr> <td>Last Modif Date </td> <td>: {{asset.modif_dtime}}</td> </tr> <tr> <td>Parent </td> <td>: {{asset.parent}}</td> </tr> <tr> <td>Content ID </td> <td>: {{asset.content}}</td> </tr> <tr> <td>Thumbnail ID </td> <td>: {{asset.thumbnail}}</td> </tr> </table>  <div layout=row> <span flex></span> <md-button class=\"md-raised md-primary\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=true\"> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> <section ng-show=ctrl.edit layout=column> <md-button class=md-raised ng-click=changeFile()> {{'change file' | translate}} </md-button> <md-input-container class=md-block flex-gt-xs> <label>Name</label> <input ng-model=asset.name> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Description</label> <input ng-model=asset.description> </md-input-container> <md-input-container ng-if=\"asset.type === 'file'\" class=md-block flex-gt-xs> <label>Price</label> <input ng-model=asset.price> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Parent</label> <input ng-model=asset.parent> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Content</label> <input ng-model=asset.content> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Thumbnail</label> <input ng-model=asset.thumbnail> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=save()> <wb-icon>save</wb-icon> <span translate>Save</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=false\"> <wb-icon>close</wb-icon> <span translate>Cancel</span> </md-button> </div> </section> </mb-titled-block>  <mb-titled-block mb-progress=ctrl.loadingCategories layout=column mb-title=\"{{'Categories' | translate}}\"> <div class=sdp-chips-list style=direction:rtl> <div layout=row layout-align=\"start center\"> <md-chips flex class=custom-chips ng-model=ctrl.categories md-removable=true> <md-chip-template> <span> <strong>{{$chip.id}} - {{$chip.name}}</strong> </span> </md-chip-template> <button md-chip-remove class=\"md-primary chipaction\" ng-click=removeFromCategory($chip)> <wb-icon class=block>close</wb-icon> </button> </md-chips> <md-button class=\"md-icon-button md-raised\" ng-click=addToCategory()> <wb-icon>add</wb-icon> </md-button> </div> <div ng-if=\"!ctrl.categories || ctrl.categories.length == 0\" layout=column layout-align=\"center center\"> <p>This asset is not contained in any category</p> </div> </div> </mb-titled-block>  <mb-titled-block mb-progress=ctrl.loadingTags mb-title=\"{{'Tags' | translate}}\" layout=column> <div class=sdp-chips-list style=direction:rtl> <div layout=row layout-align=\"start center\"> <md-chips flex class=custom-chips ng-model=ctrl.tags md-removable=true> <md-chip-template> <span> <strong>{{$chip.id}} - {{$chip.name}}</strong> </span> </md-chip-template> <button md-chip-remove class=\"md-primary chipaction\" ng-click=removeTag($chip)> <wb-icon class=block>close</wb-icon> </button> </md-chips> <md-button class=\"md-icon-button md-raised\" ng-click=addTag()> <wb-icon>add</wb-icon> </md-button> </div> <div ng-show=\"!ctrl.tags || ctrl.tags.length == 0\" layout=column layout-align=\"center center\"> <p>This asset has no any tag.</p> </div> </div> </mb-titled-block>  <mb-titled-block mb-progress=ctrl.loadingRelateds mb-title=\"{{'Relateds' | translate}}\" layout=column> <div class=sdp-chips-list style=direction:rtl> <div layout=row layout-align=\"start center\"> <md-chips flex class=custom-chips ng-model=ctrl.relateds md-on-select=goToAssetPage($chip.id) md-removable=true style=direction:rtl> <md-chip-template> <span> <strong>{{$chip.id}} - {{$chip.name}}</strong> </span> </md-chip-template> <button md-chip-remove class=\"md-primary chipaction\" ng-click=removeRelation($chip)> <wb-icon class=block>close</wb-icon> </button> </md-chips> <md-button class=\"md-icon-button md-raised\" ng-click=addRelation()> <wb-icon>add</wb-icon> </md-button> </div> <div ng-show=\"!ctrl.relateds || ctrl.relateds.length == 0\" layout=column layout-align=\"center center\"> <p>No one asset is related to this asset.</p> </div> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-assets.html',
    " <md-content mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column ng-if=\"ctrl.items.length == 0 && !ctrl.loadingItems\"> <p style=\"text-align: center\" translate>List is empty.</p> <div layout=row layout-align=\"center center\"> <md-button class=md-raised ng-click=add()> <wb-icon>add</wb-icon> <span translate>New asset</span> </md-button> </div> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'sdp/asset/'+pobject.id}}\"> <wb-icon>folder</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=ctrl.loadingItems md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-content>"
  );


  $templateCache.put('views/sdp-categories.html',
    " <md-content mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column ng-if=\"ctrl.items.length == 0 && !ctrl.loading\"> <p style=\"text-align: center\" translate>List is empty.</p> <div layout=row layout-align=\"center center\"> <md-button class=md-raised ng-click=add()> <wb-icon>add</wb-icon> <span translate>New category</span> </md-button> </div> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'sdp/category/'+pobject.id}}\"> <wb-icon>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=ctrl.loading md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-content>"
  );


  $templateCache.put('views/sdp-category-new.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=ctrl.saving mb-title=\"{{'New category'| translate}}\" layout=column> <form name=categoryForm ng-action=add(config) layout=column flex> <md-input-container> <label>Name</label> <input ng-model=config.model.name required> </md-input-container> <md-input-container> <label>Description</label> <input ng-model=config.model.description> </md-input-container> <div layout=row layout-align=\"start center\"> <md-input-container flex> <label>Parent</label> <input ng-model=ctrl.parent_id> </md-input-container> <md-button class=\"md-icon-button md-raised\" ng-click=selectParent()> <wb-icon>touch_app</wb-icon> </md-button> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel()>  <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=add(config)>  <span translate=\"\">Add</span> </md-button> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-category.html',
    "<md-content layout-margin flex mb-infinate-scroll=nextChilds()> <mb-titled-block mb-progress=\"ctrl.loadingCategory || ctrl.savingCategory\" mb-title=\"{{'Category'| translate}}\" layout=column> <section ng-show=!ctrl.edit layout=column> <table> <tr> <td>ID </td> <td>: {{category.id}}</td> </tr> <tr> <td>Name </td> <td>: {{category.name}}</td> </tr> <tr> <td>Description </td> <td>: {{category.description}}</td> </tr> <tr> <td>Creation Date </td> <td>: {{category.creation_dtime}}</td> </tr> <tr> <td>Last Modif Date </td> <td>: {{category.modif_dtime}}</td> </tr> <tr> <td>Parent </td> <td>: {{category.parent_id}}</td> </tr> <tr> <td>Content ID </td> <td>: {{category.content}}</td> </tr> <tr> <td>Thumbnial ID </td> <td>: {{category.thumbnail}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()>  <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=true\">  <span translate>Edit</span> </md-button> </div> </section> <section ng-show=ctrl.edit layout=column> <md-input-container class=md-block flex-gt-xs> <label>Name</label> <input ng-model=category.name> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Description</label> <input ng-model=category.description> </md-input-container> <div layout=row> <md-input-container class=md-block flex-gt-xs> <label>Parent</label> <input ng-model=category.parent_id> </md-input-container> <md-button class=\"md-icon-button md-raised\" ng-click=selectParent()> <wb-icon>touch_app</wb-icon> </md-button> </div> <md-input-container class=md-block flex-gt-xs> <label>Content</label> <input ng-model=category.content> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Thumbnail</label> <input ng-model=category.thumbnail> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=\"ctrl.edit=false\">  <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=save()>  <span translate>Save</span> </md-button> </div> </section> </mb-titled-block>  <mb-titled-block mb-progress=ctrl.loadingChilds mb-title=\"{{'Subcategories' | translate}}\" layout=column> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.childs track by pobject.id\" class=md-3-line ng-href=\"{{'sdp/category/'+pobject.id}}\"> <wb-icon>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-downloaded-files-link.html',
    "<md-content mb-infinate-scroll=nextPage() layout=column flex mb-preloading=mainWaiting>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <div layout=row layout-sm=column layout-align=\"center center\"> <md-progress-circular ng-show=loadingLinks md-diameter=96> Loading ... </md-progress-circular> </div> <md-list flex ng-if=\"items.length > 0\"> <sdp-downloaded-link ng-repeat=\"item in items\" item=item> </sdp-downloaded-link> </md-list> <div layout=column ng-if=flag> <h3 style=\"text-align: center\" translate>Noting found</h3> </div> </md-content>"
  );


  $templateCache.put('views/sdp-tag-new.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=ctrl.saving mb-title=\"{{'New tag'| translate}}\" layout=column> <form name=tagForm ng-action=add(config) layout=column flex> <md-input-container> <label>Name</label> <input ng-model=config.model.name> </md-input-container> <md-input-container> <label>Description</label> <input ng-model=config.model.description> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel()>  <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=add(config)>  <span translate>Add</span> </md-button> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-tag.html',
    "<md-content layout-margin flex> <mb-titled-block mb-progress=\"ctrl.loadingTag || ctrl.savingTag\" mb-title=\"{{'Tag'| translate}}\" layout=column> <section ng-show=!ctrl.edit layout=column> <table> <tr> <td>ID </td> <td>: {{tag.id}}</td> </tr> <tr> <td>Name </td> <td>: {{tag.name}}</td> </tr> <tr> <td>Description </td> <td>: {{tag.description}}</td> </tr> <tr> <td>Creation Date</td> <td>: {{tag.creation_dtime}}</td> </tr> <tr> <td>Last Modif Date</td> <td>: {{tag.modif_dtime}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()>  <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=true\">  <span translate>Edit</span> </md-button> </div> </section> <section ng-show=ctrl.edit layout=column> <md-input-container class=md-block flex-gt-xs> <label>Name</label> <input ng-model=tag.name> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Description</label> <input ng-model=tag.description> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=\"ctrl.edit=false\">  <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=save()>  <span translate>Save</span> </md-button> </div> </section> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/sdp-tags.html',
    " <md-content mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'sdp/tag/'+pobject.id}}\"> <wb-icon>label</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}}</h3> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=ctrl.loadingItems md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-content>"
  );

}]);
